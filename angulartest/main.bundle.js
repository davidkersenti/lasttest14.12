webpackJsonp([1,4],{

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvoicesService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InvoicesService = (function () {
    function InvoicesService(af) {
        this.af = af;
    }
    InvoicesService.prototype.getInvoices = function () {
        this.invoicesObservable = this.af.database.list('/invoices').delay(2000);
        return this.invoicesObservable;
    };
    InvoicesService.prototype.addInvoice = function (invoice) {
        this.invoicesObservable.push(invoice);
    };
    InvoicesService.prototype.deleteInvoice = function (invoice) {
        var invoiceKey = invoice.$key;
        this.af.database.object('/invoices/' + invoiceKey).remove();
    };
    InvoicesService.prototype.updateInvoice = function (invoice) {
        var invoice1 = { name: invoice.name, amount: invoice.amount };
        console.log(invoice1);
        this.af.database.object('/invoices/' + invoice.$key).update(invoice1);
    };
    InvoicesService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */]) === 'function' && _a) || Object])
    ], InvoicesService);
    return InvoicesService;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/invoices.service.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayersService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayersService = (function () {
    function PlayersService(af) {
        this.af = af;
    }
    PlayersService.prototype.getPlayers = function () {
        var _this = this;
        this.playersObservable = this.af.database.list('/players').map(function (players) {
            {
                players.map(function (player) {
                    player.positionName = [];
                    player.positionName.push(_this.af.database.object('/position/' + player.positionId));
                });
                return players;
            }
        });
        return this.playersObservable;
    };
    PlayersService.prototype.deletePlayer = function (player) {
        var playerKey = player.$key;
        this.af.database.object('/players/' + playerKey).remove();
    };
    PlayersService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */]) === 'function' && _a) || Object])
    ], PlayersService);
    return PlayersService;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/players.service.js.map

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2__ = __webpack_require__(68);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PostsService = (function () {
    function PostsService(af) {
        this.af = af;
    }
    PostsService.prototype.getPosts = function () {
        var _this = this;
        this.postsObservable = this.af.database.list('/posts').map(function (posts) {
            posts.map(function (post) {
                post.userNames = [];
                for (var u in post.users) {
                    post.userNames.push(_this.af.database.object('/users/' + u));
                }
            });
            return posts;
        });
        return this.postsObservable;
    };
    PostsService.prototype.addPost = function (post) {
        this.postsObservable.push(post);
    };
    PostsService.prototype.updatePost = function (post) {
        var postKey = post.$key;
        var postData = { id: post.id, title: post.title };
        this.af.database.object('/posts/' + postKey).update(postData);
    };
    PostsService.prototype.deletePost = function (post) {
        var postKey = post.$key;
        this.af.database.object('/posts/' + postKey).remove();
    };
    PostsService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3_angularfire2__["b" /* AngularFire */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_angularfire2__["b" /* AngularFire */]) === 'function' && _a) || Object])
    ], PostsService);
    return PostsService;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/posts.service.js.map

/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductsService = (function () {
    function ProductsService(af) {
        this.af = af;
    }
    ProductsService.prototype.getProducts = function () {
        var _this = this;
        this.productsObservable = this.af.database.list('/products').map(function (products) {
            {
                products.map(function (product) {
                    product.categoryName = [];
                    product.categoryName.push(_this.af.database.object('/category/' + product.categoryId));
                });
                return products;
            }
        });
        return this.productsObservable;
    };
    ProductsService.prototype.deleteProduct = function (product) {
        var productKey = product.$key;
        this.af.database.object('/products/' + productKey).remove();
    };
    ProductsService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */]) === 'function' && _a) || Object])
    ], ProductsService);
    return ProductsService;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/products.service.js.map

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2__ = __webpack_require__(68);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsersService = (function () {
    function UsersService(af) {
        this.af = af;
    }
    UsersService.prototype.addUser = function (user) {
        this.usersObservable.push(user);
    };
    UsersService.prototype.getUsers = function () {
        var _this = this;
        this.usersObservable = this.af.database.list('/users').map(function (users) {
            users.map(function (user) {
                user.posTitles = [];
                for (var p in user.posts) {
                    user.posTitles.push(_this.af.database.object('/posts/' + p));
                }
            });
            return users;
        });
        //this.usersObservable = this.af.database.list('/users');
        return this.usersObservable;
    };
    UsersService.prototype.deleteUser = function (user) {
        var userKey = user.$key;
        this.af.database.object('/users/' + userKey).remove();
    };
    UsersService.prototype.updateUser = function (user) {
        var user1 = { name: user.name, email: user.email };
        console.log(user1);
        this.af.database.object('/users/' + user.$key).update(user1);
    };
    UsersService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3_angularfire2__["b" /* AngularFire */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_angularfire2__["b" /* AngularFire */]) === 'function' && _a) || Object])
    ], UsersService);
    return UsersService;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/users.service.js.map

/***/ }),

/***/ 404:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 404;


/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(525);




if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=C:/angular-test/src/main.js.map

/***/ }),

/***/ 524:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2__ = __webpack_require__(68);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(af) {
        this.title = 'app works!';
        console.log(af);
    }
    ;
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(724),
            styles: [__webpack_require__(709)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_angularfire2__["b" /* AngularFire */]) === 'function' && _a) || Object])
    ], AppComponent);
    return AppComponent;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/app.component.js.map

/***/ }),

/***/ 525:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(489);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(524);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__users_users_component__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__users_users_service__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_router__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__page_not_found_page_not_found_component__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__user_form_user_form_component__ = __webpack_require__(539);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__user_user_component__ = __webpack_require__(540);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__products_products_component__ = __webpack_require__(537);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__products_products_service__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__product_product_component__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__posts_posts_component__ = __webpack_require__(535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__posts_posts_service__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__post_post_component__ = __webpack_require__(534);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__invoices_invoices_component__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__invoices_invoices_service__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__invoice_form_invoice_form_component__ = __webpack_require__(526);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__invoice_invoice_component__ = __webpack_require__(527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__shared_spinner_spinner_component__ = __webpack_require__(538);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__players_players_component__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__players_players_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__player_player_component__ = __webpack_require__(530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__position_position_component__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__post_form_post_form_component__ = __webpack_require__(533);
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




























var firebaseConfig = {
    apiKey: "AIzaSyAybWqWd2Dbr6A6TPi3aZF-t3l5kfSAPoo",
    authDomain: "users-352f2.firebaseapp.com",
    databaseURL: "https://users-352f2.firebaseio.com",
    projectId: "users-352f2",
    storageBucket: "users-352f2.appspot.com",
    messagingSenderId: "914352984198"
};
var appRoutes = [
    { path: 'users', component: __WEBPACK_IMPORTED_MODULE_6__users_users_component__["a" /* UsersComponent */] },
    { path: 'userForm', component: __WEBPACK_IMPORTED_MODULE_6__users_users_component__["a" /* UsersComponent */] },
    { path: 'products', component: __WEBPACK_IMPORTED_MODULE_12__products_products_component__["a" /* ProductsComponent */] },
    { path: 'players', component: __WEBPACK_IMPORTED_MODULE_23__players_players_component__["a" /* PlayersComponent */] },
    { path: 'invoices', component: __WEBPACK_IMPORTED_MODULE_18__invoices_invoices_component__["a" /* InvoicesComponent */] },
    { path: 'invoiceForm', component: __WEBPACK_IMPORTED_MODULE_20__invoice_form_invoice_form_component__["a" /* InvoiceFormComponent */] },
    { path: 'posts', component: __WEBPACK_IMPORTED_MODULE_15__posts_posts_component__["a" /* PostsComponent */] },
    { path: '', component: __WEBPACK_IMPORTED_MODULE_23__players_players_component__["a" /* PlayersComponent */] },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_9__page_not_found_page_not_found_component__["a" /* PageNotFoundComponent */] }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__users_users_component__["a" /* UsersComponent */],
                __WEBPACK_IMPORTED_MODULE_9__page_not_found_page_not_found_component__["a" /* PageNotFoundComponent */],
                __WEBPACK_IMPORTED_MODULE_11__user_user_component__["a" /* UserComponent */],
                __WEBPACK_IMPORTED_MODULE_10__user_form_user_form_component__["a" /* UserFormComponent */],
                __WEBPACK_IMPORTED_MODULE_12__products_products_component__["a" /* ProductsComponent */],
                __WEBPACK_IMPORTED_MODULE_14__product_product_component__["a" /* ProductComponent */],
                __WEBPACK_IMPORTED_MODULE_15__posts_posts_component__["a" /* PostsComponent */],
                __WEBPACK_IMPORTED_MODULE_17__post_post_component__["a" /* PostComponent */],
                __WEBPACK_IMPORTED_MODULE_18__invoices_invoices_component__["a" /* InvoicesComponent */],
                __WEBPACK_IMPORTED_MODULE_20__invoice_form_invoice_form_component__["a" /* InvoiceFormComponent */],
                __WEBPACK_IMPORTED_MODULE_21__invoice_invoice_component__["a" /* InvoiceComponent */],
                __WEBPACK_IMPORTED_MODULE_22__shared_spinner_spinner_component__["a" /* SpinnerComponent */],
                __WEBPACK_IMPORTED_MODULE_23__players_players_component__["a" /* PlayersComponent */],
                __WEBPACK_IMPORTED_MODULE_25__player_player_component__["a" /* PlayerComponent */],
                __WEBPACK_IMPORTED_MODULE_26__position_position_component__["a" /* PositionComponent */],
                __WEBPACK_IMPORTED_MODULE_27__post_form_post_form_component__["a" /* PostFormComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_5_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_8__angular_router__["a" /* RouterModule */].forRoot(appRoutes)
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_7__users_users_service__["a" /* UsersService */], __WEBPACK_IMPORTED_MODULE_13__products_products_service__["a" /* ProductsService */], __WEBPACK_IMPORTED_MODULE_16__posts_posts_service__["a" /* PostsService */], __WEBPACK_IMPORTED_MODULE_19__invoices_invoices_service__["a" /* InvoicesService */], __WEBPACK_IMPORTED_MODULE_24__players_players_service__["a" /* PlayersService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=C:/angular-test/src/app.module.js.map

/***/ }),

/***/ 526:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvoiceFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InvoiceFormComponent = (function () {
    function InvoiceFormComponent() {
        this.invoiceAddedEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.invoice = {
            name: '',
            amount: '1000'
        };
    }
    InvoiceFormComponent.prototype.onSubmit = function (form) {
        console.log(form);
        this.invoiceAddedEvent.emit(this.invoice);
        this.invoice = {
            name: '',
            amount: ''
        };
    };
    InvoiceFormComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], InvoiceFormComponent.prototype, "invoiceAddedEvent", void 0);
    InvoiceFormComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-invoice-form',
            template: __webpack_require__(725),
            styles: [__webpack_require__(710)]
        }), 
        __metadata('design:paramtypes', [])
    ], InvoiceFormComponent);
    return InvoiceFormComponent;
}());
//# sourceMappingURL=C:/angular-test/src/invoice-form.component.js.map

/***/ }),

/***/ 527:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvoiceComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InvoiceComponent = (function () {
    function InvoiceComponent() {
        this.deleteEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.editEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.isEdit = false;
        this.editButtonText = 'Edit';
        this.tempInvoice = { amount: null, name: null };
    }
    InvoiceComponent.prototype.sendDelete = function () {
        this.deleteEvent.emit(this.invoice);
    };
    InvoiceComponent.prototype.cancelEdit = function () {
        this.isEdit = false;
        this.invoice.amount = this.tempInvoice.amount;
        this.invoice.name = this.tempInvoice.name;
        this.editButtonText = 'Edit';
    };
    InvoiceComponent.prototype.toggleEdit = function () {
        this.isEdit = !this.isEdit;
        this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
        if (!this.isEdit) {
            this.editEvent.emit(this.invoice);
        }
    };
    InvoiceComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], InvoiceComponent.prototype, "deleteEvent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], InvoiceComponent.prototype, "editEvent", void 0);
    InvoiceComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-invoice',
            template: __webpack_require__(726),
            styles: [__webpack_require__(711)],
            inputs: ['invoice']
        }), 
        __metadata('design:paramtypes', [])
    ], InvoiceComponent);
    return InvoiceComponent;
}());
//# sourceMappingURL=C:/angular-test/src/invoice.component.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__invoices_service__ = __webpack_require__(341);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvoicesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InvoicesComponent = (function () {
    function InvoicesComponent(_invoicesService) {
        this._invoicesService = _invoicesService;
        this.isLoading = true;
        //this.users = this._userService.getUsers();
    }
    InvoicesComponent.prototype.addInvoice = function (invoice) {
        this._invoicesService.addInvoice(invoice);
    };
    InvoicesComponent.prototype.deleteInvoice = function (invoice) {
        //this.users.splice(    // we delete this because we want delete from firebase 
        //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
        //  )
        this._invoicesService.deleteInvoice(invoice);
    };
    InvoicesComponent.prototype.updateInvoice = function (invoice) {
        this._invoicesService.updateInvoice(invoice);
    };
    InvoicesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._invoicesService.getInvoices()
            .subscribe(function (invoices) {
            _this.invoices = invoices;
            _this.isLoading = false;
        });
    };
    InvoicesComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-invoices',
            template: __webpack_require__(727),
            styles: ["\n    .invoices li { cursor: default; }\n    .invoices li:hover { background: #ecf0f1; } \n  "]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__invoices_service__["a" /* InvoicesService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__invoices_service__["a" /* InvoicesService */]) === 'function' && _a) || Object])
    ], InvoicesComponent);
    return InvoicesComponent;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/invoices.component.js.map

/***/ }),

/***/ 529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageNotFoundComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageNotFoundComponent = (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-page-not-found',
            template: __webpack_require__(728),
            styles: [__webpack_require__(712)]
        }), 
        __metadata('design:paramtypes', [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());
//# sourceMappingURL=C:/angular-test/src/page-not-found.component.js.map

/***/ }),

/***/ 530:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlayerComponent = (function () {
    function PlayerComponent() {
        this.deleteEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
    }
    PlayerComponent.prototype.sendDelete = function () {
        this.deleteEvent.emit(this.player);
    };
    PlayerComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], PlayerComponent.prototype, "deleteEvent", void 0);
    PlayerComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-player',
            template: __webpack_require__(729),
            styles: [__webpack_require__(713)],
            inputs: ['player']
        }), 
        __metadata('design:paramtypes', [])
    ], PlayerComponent);
    return PlayerComponent;
}());
//# sourceMappingURL=C:/angular-test/src/player.component.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__players_service__ = __webpack_require__(342);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlayersComponent = (function () {
    function PlayersComponent(_playersService) {
        this._playersService = _playersService;
        this.isLoading = true;
        //this.users = this._userService.getUsers();
    }
    PlayersComponent.prototype.deletePlayer = function (player) {
        //this.users.splice(    // we delete this because we want delete from firebase 
        //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
        //  )
        this._playersService.deletePlayer(player);
    };
    PlayersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._playersService.getPlayers()
            .subscribe(function (players) {
            _this.players = players;
            _this.isLoading = false;
        });
    };
    PlayersComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-players',
            template: __webpack_require__(730),
            styles: [__webpack_require__(714)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__players_service__["a" /* PlayersService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__players_service__["a" /* PlayersService */]) === 'function' && _a) || Object])
    ], PlayersComponent);
    return PlayersComponent;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/players.component.js.map

/***/ }),

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PositionComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PositionComponent = (function () {
    function PositionComponent() {
    }
    PositionComponent.prototype.ngOnInit = function () {
    };
    PositionComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-position',
            template: __webpack_require__(731),
            styles: [__webpack_require__(715)]
        }), 
        __metadata('design:paramtypes', [])
    ], PositionComponent);
    return PositionComponent;
}());
//# sourceMappingURL=C:/angular-test/src/position.component.js.map

/***/ }),

/***/ 533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PostFormComponent = (function () {
    function PostFormComponent() {
        this.postAddedEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.post = {
            id: '',
            title: ''
        };
    }
    PostFormComponent.prototype.onSubmit = function (form) {
        console.log(form);
        this.postAddedEvent.emit(this.post);
        this.post = {
            id: '',
            title: ''
        };
    };
    PostFormComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], PostFormComponent.prototype, "postAddedEvent", void 0);
    PostFormComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-post-form',
            template: __webpack_require__(732),
            styles: [__webpack_require__(716)]
        }), 
        __metadata('design:paramtypes', [])
    ], PostFormComponent);
    return PostFormComponent;
}());
//# sourceMappingURL=C:/angular-test/src/post-form.component.js.map

/***/ }),

/***/ 534:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PostComponent = (function () {
    function PostComponent() {
        this.deleteEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.editEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.tempPost = { id: null, title: null };
        this.isEdit = false;
        this.editButtonText = 'Edit';
    }
    PostComponent.prototype.sendDelete = function () {
        this.deleteEvent.emit(this.post);
    };
    PostComponent.prototype.cancelEdit = function () {
        this.isEdit = false;
        this.post.title = this.tempPost.title;
        this.post.id = this.tempPost.id;
        this.editButtonText = 'Edit';
    };
    PostComponent.prototype.toggleEdit = function () {
        //update parent about the change
        this.isEdit = !this.isEdit;
        this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
        if (this.isEdit) {
            this.tempPost.title = this.post.title;
            this.tempPost.id = this.post.id;
        }
        else {
            var originalAndNew = [];
            originalAndNew.push(this.tempPost, this.post);
            this.editEvent.emit(originalAndNew);
        }
    };
    PostComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], PostComponent.prototype, "deleteEvent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], PostComponent.prototype, "editEvent", void 0);
    PostComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-post',
            template: __webpack_require__(733),
            styles: [__webpack_require__(717)],
            inputs: ['post']
        }), 
        __metadata('design:paramtypes', [])
    ], PostComponent);
    return PostComponent;
}());
//# sourceMappingURL=C:/angular-test/src/post.component.js.map

/***/ }),

/***/ 535:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__posts_service__ = __webpack_require__(343);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PostsComponent = (function () {
    function PostsComponent(_postsService) {
        this._postsService = _postsService;
        this.isLoading = true;
        //  this.posts = this._postsService.getPosts();
    }
    PostsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._postsService.getPosts()
            .subscribe(function (posts) {
            _this.posts = posts;
            _this.isLoading = false;
        });
    };
    PostsComponent.prototype.addPost = function (post) {
        this._postsService.addPost(post);
    };
    PostsComponent.prototype.select = function (post) {
        this.currentPost = post;
        console.log(this.currentPost);
    };
    PostsComponent.prototype.updatePost = function (post) {
        //this.posts.splice(
        // this.posts.indexOf(post),1,post)
        this._postsService.updatePost(post);
    };
    PostsComponent.prototype.deletePost = function (post) {
        //this.posts.splice(
        //this.posts.indexOf(post),1)
        this._postsService.deletePost(post);
    };
    PostsComponent.prototype.editPost = function (originalAndEdited) {
        this.posts.splice(this.posts.indexOf(originalAndEdited[0]), 1, originalAndEdited[1]);
        console.log(this.posts);
    };
    PostsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-posts',
            template: __webpack_require__(734),
            styles: ["\n    .users li { cursor: default; }\n    .users li:hover { background: #ecf0f1; }\n    .list-group-item.active, \n    .list-group-item.active:hover { \n         background-color: #ecf0f1;\n         border-color: #ecf0f1; \n         color: #2c3e50;\n    }     \n  "]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__posts_service__["a" /* PostsService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__posts_service__["a" /* PostsService */]) === 'function' && _a) || Object])
    ], PostsComponent);
    return PostsComponent;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/posts.component.js.map

/***/ }),

/***/ 536:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductComponent = (function () {
    function ProductComponent() {
        this.deleteEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
    }
    ProductComponent.prototype.sendDelete = function () {
        this.deleteEvent.emit(this.product);
    };
    ProductComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], ProductComponent.prototype, "deleteEvent", void 0);
    ProductComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-product',
            template: __webpack_require__(735),
            styles: [__webpack_require__(718)],
            inputs: ['product']
        }), 
        __metadata('design:paramtypes', [])
    ], ProductComponent);
    return ProductComponent;
}());
//# sourceMappingURL=C:/angular-test/src/product.component.js.map

/***/ }),

/***/ 537:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__products_service__ = __webpack_require__(344);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductsComponent = (function () {
    function ProductsComponent(_productsService) {
        this._productsService = _productsService;
        this.isLoading = true;
        //this.users = this._userService.getUsers();
    }
    ProductsComponent.prototype.deleteProduct = function (prodcut) {
        //this.users.splice(    // we delete this because we want delete from firebase 
        //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
        //  )
        this._productsService.deleteProduct(prodcut);
    };
    ProductsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._productsService.getProducts()
            .subscribe(function (products) {
            _this.products = products;
            _this.isLoading = false;
        });
    };
    ProductsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-products',
            template: __webpack_require__(736),
            styles: [__webpack_require__(719)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__products_service__["a" /* ProductsService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__products_service__["a" /* ProductsService */]) === 'function' && _a) || Object])
    ], ProductsComponent);
    return ProductsComponent;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/products.component.js.map

/***/ }),

/***/ 538:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpinnerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SpinnerComponent = (function () {
    function SpinnerComponent() {
    }
    SpinnerComponent.prototype.ngOnInit = function () {
    };
    SpinnerComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-spinner',
            template: __webpack_require__(737),
            styles: [__webpack_require__(720)],
            inputs: ['visible']
        }), 
        __metadata('design:paramtypes', [])
    ], SpinnerComponent);
    return SpinnerComponent;
}());
//# sourceMappingURL=C:/angular-test/src/spinner.component.js.map

/***/ }),

/***/ 539:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserFormComponent = (function () {
    //usr:User = {name:'',email:''};  
    function UserFormComponent() {
        this.userAddedEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.user = {
            name: '',
            email: ''
        };
    }
    UserFormComponent.prototype.onSubmit = function (form) {
        console.log(form);
        this.userAddedEvent.emit(this.user);
        this.user = {
            name: '',
            email: ''
        };
    };
    UserFormComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], UserFormComponent.prototype, "userAddedEvent", void 0);
    UserFormComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-user-form',
            template: __webpack_require__(738),
            styles: [__webpack_require__(721)]
        }), 
        __metadata('design:paramtypes', [])
    ], UserFormComponent);
    return UserFormComponent;
}());
//# sourceMappingURL=C:/angular-test/src/user-form.component.js.map

/***/ }),

/***/ 540:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserComponent = (function () {
    function UserComponent() {
        this.deleteEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.editEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* EventEmitter */]();
        this.isEdit = false;
        this.editButtonText = 'Edit';
        this.tempUser = { email: null, name: null };
    }
    UserComponent.prototype.sendDelete = function () {
        this.deleteEvent.emit(this.user);
    };
    UserComponent.prototype.toggleEdit = function () {
        this.isEdit = !this.isEdit;
        this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
        if (!this.isEdit) {
            this.editEvent.emit(this.user);
        }
    };
    UserComponent.prototype.cancelEdit = function () {
        this.isEdit = false;
        this.user.email = this.tempUser.email;
        this.user.name = this.tempUser.name;
        this.editButtonText = 'Edit';
    };
    UserComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], UserComponent.prototype, "deleteEvent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Output */])(), 
        __metadata('design:type', Object)
    ], UserComponent.prototype, "editEvent", void 0);
    UserComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-user',
            template: __webpack_require__(739),
            styles: [__webpack_require__(722)],
            inputs: ['user']
        }), 
        __metadata('design:paramtypes', [])
    ], UserComponent);
    return UserComponent;
}());
//# sourceMappingURL=C:/angular-test/src/user.component.js.map

/***/ }),

/***/ 541:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__users_service__ = __webpack_require__(345);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UsersComponent = (function () {
    function UsersComponent(_usersService) {
        this._usersService = _usersService;
        this.isLoading = true;
        //this.users = this._userService.getUsers();
    }
    UsersComponent.prototype.addUser = function (user) {
        this._usersService.addUser(user);
    };
    UsersComponent.prototype.deleteUser = function (user) {
        //this.users.splice(    // we delete this because we want delete from firebase 
        //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
        //  )
        this._usersService.deleteUser(user);
    };
    UsersComponent.prototype.updateUser = function (user) {
        this._usersService.updateUser(user);
    };
    UsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._usersService.getUsers()
            .subscribe(function (users) {
            _this.users = users;
            _this.isLoading = false;
            console.log(users);
        });
    };
    UsersComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
            selector: 'app-users',
            template: __webpack_require__(740),
            styles: [__webpack_require__(723)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__users_service__["a" /* UsersService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__users_service__["a" /* UsersService */]) === 'function' && _a) || Object])
    ], UsersComponent);
    return UsersComponent;
    var _a;
}());
//# sourceMappingURL=C:/angular-test/src/users.component.js.map

/***/ }),

/***/ 542:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=C:/angular-test/src/environment.js.map

/***/ }),

/***/ 709:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 710:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 711:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 712:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 713:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 714:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 715:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 716:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 717:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 718:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 719:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 720:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 721:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 722:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 723:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 724:
/***/ (function(module, exports) {

module.exports = "<h1>\n  {{title}}\n</h1>\n\n<nav>\n  <a routerLink='/users'>Users</a> | \n  <a routerLink='/userForm'>UserForm</a> |\n  <a routerLink='/products'>Products</a> |\n  <a routerLink='/joins'>Joins</a> |\n  <a routerLink='/posts'>Posts</a> |\n  <a routerLink='/invoiceForm'>Invoice Form</a>  |\n  <a routerLink='/invoices'>Invoices</a> | \n  <a routerLink='/players'>Players</a> \n \n \n\n  \n</nav> <br/>\n<router-outlet></router-outlet>\n\n"

/***/ }),

/***/ 725:
/***/ (function(module, exports) {

module.exports = "<h2>Add Invoice</h2>\n<form (ngSubmit) = \"onSubmit(f)\" #f = \"ngForm\">\n  <div class=\"form-group\">\n    <label for=\"name\">Product Name</label>\n    <input type=\"text\"\n            class=\"form-control\"\n            id=\"name\"\n            name = \"name\"\n            required\n            [(ngModel)] = \"invoice.name\">\n  </div>\n  <div class=\"form-group\">\n    <label for=\"amount\">Amount</label>\n    <input type=\"text\"\n            class=\"form-control\"\n            id=\"amount\"\n            name = \"amount\" \n            required\n           \n            [(ngModel)] = \"invoice.amount\">\n               \n  </div>\n  <button type=\"submit\" class=\"btn btn-primary\" >Save Invoice</button>\n</form>"

/***/ }),

/***/ 726:
/***/ (function(module, exports) {

module.exports = "Name: {{invoice.name}}, Amount {{invoice.amount}}\n<span *ngIf = \"!isEdit\" >Name: {{invoice.name}}, Amount {{invoice.amount}} </span>\n\n<span *ngIf = \"isEdit\" >Name: <input type=\"text\" [(ngModel)] = \"invoice.name\"> Amount: <input type=\"text\" [(ngModel)] = \"invoice.amount\"></span>\n<button *ngIf = \"!isEdit\" (click) = \"sendDelete()\" >Delete</button> <button (click) = \"toggleEdit()\" >{{editButtonText}}</button> \n<button *ngIf = \"isEdit\" (click) = \"cancelEdit()\">Cancel</button> "

/***/ }),

/***/ 727:
/***/ (function(module, exports) {

module.exports = "<app-invoice-form (invoiceAddedEvent) = \"addInvoice($event)\"></app-invoice-form>\n<div>\n  <h3>Invoices list:</h3>\n  <app-spinner [visible] = \"isLoading\"></app-spinner>\n  <ul class=\"list-group invoices\">\n        <li class=\"list-group-item\" *ngFor = \"let invoice of invoices\" >\n            \n             <app-invoice [invoice]=\"invoice\" (deleteEvent) = \"deleteInvoice($event)\" (editEvent) = \"updateInvoice($event)\"></app-invoice>          \n        </li>\n  </ul>\n</div>"

/***/ }),

/***/ 728:
/***/ (function(module, exports) {

module.exports = "<p>\n  page-not-found works!\n</p>\n"

/***/ }),

/***/ 729:
/***/ (function(module, exports) {

module.exports = "{{player.pid}}\n          {{player.pname}}\n          {{player.cost}}\n          {{player.positionId}}\n          <button (click) = \"sendDelete()\" >Delete</button>"

/***/ }),

/***/ 730:
/***/ (function(module, exports) {

module.exports = "<div>\n  <h3>Players List:</h3>\n    <table class = \"table table-striped\">\n      <thead>\n        <tr>\n          <th>Pid</th>\n          <th>Pname</th>\n          <th>Cost</th>\n          <th>PositionId</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor = \"let player of players\">\n          <td>{{player.pid}}</td>\n          <td>{{player.pname}}</td>\n          <td>{{player.cost}}</td>\n          <td>{{player.positionId}}</td>\n          <td>  <app-player [player] = player (deleteEvent) = \"deletePlayer($event)\"></app-player>  </td>\n          </tr>\n      </tbody>\n    </table>\n\n\n\n  </div>\n  <div>\n  <h3>Players List:</h3>\n    <table class = \"table table-striped\">\n      <thead>\n        <tr>\n          <th>Pid</th>\n          <th>Pname</th>\n          <th>Cost</th>\n          <th>PositionId</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor = \"let player of players\">\n          <td>{{player.pid}}</td>\n          <td>{{player.pname}}</td>\n          <td>{{player.cost}}</td>\n           \n         <td *ngFor = \"let players of player.positionName\">\n            {{(players | async)?.cname}}\n          </td>\n         \n          <td>  <app-player [player] = player (deleteEvent) = \"deletePlayer($event)\"></app-player>  </td>\n          </tr>\n      </tbody>\n    </table>\n\n\n\n  </div>"

/***/ }),

/***/ 731:
/***/ (function(module, exports) {

module.exports = "<p>\n  position works!\n</p>\n"

/***/ }),

/***/ 732:
/***/ (function(module, exports) {

module.exports = "<h2>Add Post</h2>\n<form (ngSubmit) = \"onSubmit(f)\" #f = \"ngForm\">\n  <div class=\"form-group\">\n    <label for=\"id\">Id</label>\n    <input type=\"text\"\n            class=\"form-control\"\n            id=\"id\"\n            name = \"id\"\n            required\n            [(ngModel)] = \"post.id\">\n  </div>\n  <div class=\"form-group\">\n    <label for=\"title\">Title</label>\n    <input type=\"text\"\n            class=\"form-control\"\n            id=\"title\"\n            name = \"title\" \n            required\n           \n            [(ngModel)] = \"post.title\">\n          \n             \n  </div>\n  <button type=\"submit\" class=\"btn btn-primary\" >Submit</button>\n</form>"

/***/ }),

/***/ 733:
/***/ (function(module, exports) {

module.exports = "<span *ngIf = \"!isEdit\"> id: {{post.id}}, Title: {{post.title}} </span>\n<span *ngIf = \"isEdit\"> \n    Id: <input type = \"text\" [(ngModel)] = \"post.id\" >\n    Title: <input type = \"text\" [(ngModel)] = \"post.title\">\n  \n</span>\n<button (click) = \"toggleEdit()\" > {{editButtonText}} </button>\n<button (click) = \"sendDelete()\" > Delete </button>\n  <button *ngIf = \"isEdit\" (click) = \"cancelEdit()\">Cancel</button> "

/***/ }),

/***/ 734:
/***/ (function(module, exports) {

module.exports = "<div>\n   <hr>\n   <app-post-form (postAddedEvent) = \"addPost($event)\"></app-post-form>\n   <h4>Post list</h4>\n   <app-spinner [visible] = \"isLoading\"></app-spinner>\n   <ul class=\"list-group posts\"> \n    <li *ngFor=\"let post of posts\"  \n    [class.active]=\"currentPost == post\"\n    (click)=\"select(post)\" \n    class=\"list-group-item\">\n      <app-post [post] = post (deleteEvent) = \"deletePost($event)\" (editEvent) = \"editPost($event)\"></app-post> \n\n      <ul>\n      <li *ngFor=\"let email of post.userNames\">\n        {{(email | async)?.email}}\n      </li>\n    </ul>\n      \n         \n     </li>\n   </ul>\n   <hr>\n </div>"

/***/ }),

/***/ 735:
/***/ (function(module, exports) {

module.exports = "<button (click) = \"sendDelete()\" >Delete</button> "

/***/ }),

/***/ 736:
/***/ (function(module, exports) {

module.exports = "<div>\n  <h3>Products List withoyt join:</h3>\n      <table class=\"table table-striped\">\n         <thead>\n           <tr>\n             <th>Pid</th>\n             <th>Pname</th>\n             <th>Cost</th>\n             <th>CategoryId</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor = \"let product of products\">\n              <td>{{product.pid}}</td>\n              <td>{{product.pname}}</td>\n              <td>{{product.cost}}</td>\n              <td>{{product.categoryId}}</td>\n             <td> <app-product [product] = product (deleteEvent) = \"deleteProduct($event)\"></app-product> </td>\n              </tr>\n\n            </tbody>\n        </table>\n      \n        \n  \n<div>\n  <h3>Products list:</h3>\n   <table border=\"1\">\n        <tr>\n          <th>Pid</th>\n          <th>Pname</th>\n          <th>Cost</th>\n          <th>CategoryId</th>\n        </tr>\n      \n        <tr  *ngFor = \"let product of products\" >\n           <td> {{product.pid}} </td>\n           <td> {{product.pname}} </td> \n           <td> {{product.cost}} </td>\n          <td *ngFor = \"let products of product.categoryName\">\n            {{(products | async)?.cname}}\n          </td>\n            \n          <!-- <td *ngFor = \"let name of product.categoryNames\">\n            {{(name | async)?.cname}} </td> -->\n            <app-product [product]= \"product\" (deleteEvent)=\"deleteProduct($event)\"></app-product>           \n     </tr>\n  </table>\n</div>"

/***/ }),

/***/ 737:
/***/ (function(module, exports) {

module.exports = "<i *ngIf=\"visible\" class=\"fa fa-spinner fa-spin fa-3x\"></i>"

/***/ }),

/***/ 738:
/***/ (function(module, exports) {

module.exports = "<h2>Add User</h2>\n<form (ngSubmit) = \"onSubmit(f)\" #f = \"ngForm\">\n  <div class=\"form-group\">\n    <label for=\"name\">Name</label>\n    <input type=\"text\"\n            class=\"form-control\"\n            id=\"name\"\n            name = \"name\"\n            required\n            [(ngModel)] = \"user.name\">\n  </div>\n  <div class=\"form-group\">\n    <label for=\"email\">EMail</label>\n    <input type=\"text\"\n            class=\"form-control\"\n            id=\"email\"\n            name = \"email\" \n            required\n         \n            [(ngModel)] = \"user.email\">\n           \n          \n  </div>\n  <button type=\"submit\" class=\"btn btn-primary\" >Submit</button>\n</form>"

/***/ }),

/***/ 739:
/***/ (function(module, exports) {

module.exports = " <span *ngIf = \"!isEdit\" >Name: {{user.name}}, Email {{user.email}} </span>\n\n<span *ngIf = \"isEdit\" >Name: <input type=\"text\" [(ngModel)] = \"user.name\"> Email: <input type=\"text\" [(ngModel)] = \"user.email\"></span>  \n<button *ngIf = \"!isEdit\" (click) = \"sendDelete()\" >Delete</button> <button (click) = \"toggleEdit()\" >{{editButtonText}}</button> \n<button *ngIf = \"isEdit\" (click) = \"cancelEdit()\">Cancel</button> "

/***/ }),

/***/ 740:
/***/ (function(module, exports) {

module.exports = "\n<div>\n  <app-user-form (userAddedEvent) = \"addUser($event)\"></app-user-form>\n  <h3>Users list:</h3>\n  <ul class=\"list-group users\">\n        <li class=\"list-group-item\" *ngFor = \"let user of users\" >\n            Name: {{user.name}}, Email {{user.email}}      \n             <app-user [user]=\"user\" (deleteEvent) = \"deleteUser($event)\" (editEvent) = \"updateUser($event)\"></app-user> \n              <ul>\n\n          <li *ngFor = \"let title of user.posTitles\">\n            {{(title | async)?.title}}\n          </li>\n        </ul> \n                 \n\n        </li>\n  </ul>\n</div>"

/***/ }),

/***/ 764:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(405);


/***/ })

},[764]);
//# sourceMappingURL=main.bundle.map