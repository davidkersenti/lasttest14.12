import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';

@Injectable()
export class PlayersService {
   playersObservable;

  constructor(private af:AngularFire) { }
  getPlayers(){
    this.playersObservable = this.af.database.list('/players').map(
      players =>{
      {players.map(
         player => {
            player.positionName = [];
                player.positionName.push(
                this.af.database.object('/position/' + player.positionId));
          });
        return players;
      }}
    );
   return this.playersObservable; 
   }

   deletePlayer(player){

  let playerKey = player.$key;
  this.af.database.object('/players/' + playerKey).remove();
}

}
