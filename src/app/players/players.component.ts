import { Component, OnInit } from '@angular/core';
import {PlayersService} from './players.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {
  players;
  currentPlayer;
  isLoading = true;

 
  constructor(private _playersService: PlayersService) {

    //this.users = this._userService.getUsers();
  }
  deletePlayer(player){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )

  this._playersService.deletePlayer(player);
}

 ngOnInit() {
        this._playersService.getPlayers()
			    .subscribe(players => {this.players = players;

                               this.isLoading = false});
   }

}
