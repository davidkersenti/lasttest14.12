import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
   @Output() deleteEvent = new EventEmitter<User>();
   @Output() editEvent = new EventEmitter<User>();
   
  user:User;
isEdit : boolean = false;
  editButtonText = 'Edit';
  tempUser:User = {email:null,name:null};
  constructor() { }
     sendDelete(){
    this.deleteEvent.emit(this.user);
  }
   toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit'
    if(!this.isEdit){
      this.editEvent.emit(this.user);

    }
  }

   cancelEdit(){
    this.isEdit = false;
    this.user.email = this.tempUser.email;
    this.user.name = this.tempUser.name;
    this.editButtonText = 'Edit'; 
  }
  ngOnInit() {
  }

}
