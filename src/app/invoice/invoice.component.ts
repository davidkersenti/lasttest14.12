import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from './invoice'

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {
    @Output() deleteEvent = new EventEmitter<Invoice>();
     @Output() editEvent = new EventEmitter<Invoice>();
invoice:Invoice;
 isEdit : boolean = false;
  editButtonText = 'Edit';
  tempInvoice:Invoice = {amount:null,name:null};
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.invoice);

  }
    cancelEdit(){
    this.isEdit = false;

    this.invoice.amount = this.tempInvoice.amount;
    this.invoice.name = this.tempInvoice.name;
    this.editButtonText = 'Edit'; 
  }
    toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit'
    if(!this.isEdit){
      this.editEvent.emit(this.invoice);

    }
  }

  ngOnInit() {
  }

}
