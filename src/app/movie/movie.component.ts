import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Movie} from './movie'

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  inputs:['movie']

})
export class MovieComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Movie>();
   @Output() editEvent = new EventEmitter<Movie>();

  movie:Movie;
   isEdit : boolean = false;

  editButtonText = 'Edit';
  tempMovie:Movie = {rate:null,name:null,janer:null};
  


  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.movie);
  }
    cancelEdit(){
    this.isEdit = false;
    this.movie.rate = this.tempMovie.rate;
    this.movie.janer = this.tempMovie.janer;
    this.movie.name = this.tempMovie.name;

    this.editButtonText = 'Edit'; 
  }
     toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit'

    if(!this.isEdit){
      this.editEvent.emit(this.movie);

    }
  }

  ngOnInit() {
  }

}
