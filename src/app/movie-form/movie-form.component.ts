import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import {NgForm} from '@angular/forms';
import {Movie} from '../movie/movie'

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.component.html',
  styleUrls: ['./movie-form.component.css']
})
export class MovieFormComponent implements OnInit {
  @Output() movieAddedEvent = new EventEmitter<Movie>();

  movie:Movie = {
    name: '',
    rate: '3',
    janer: ''

  };

  constructor() { }
   onSubmit(form:NgForm){
    console.log(form);
    this.movieAddedEvent.emit(this.movie);
    this.movie = {
       name: '',
       rate: '3',
       janer: ''


    }
  }

  ngOnInit() {
  }

}
