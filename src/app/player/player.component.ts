import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Player} from './player'

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
   inputs:['player']
 
})
export class PlayerComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Player>();
   player:Player;

  constructor() { }
   sendDelete(){
    this.deleteEvent.emit(this.player);
  }

  ngOnInit() {
  }

}
