import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;
  currentProduct;
  isLoading = true;

  constructor(private _productsService: ProductsService) {
    //this.users = this._userService.getUsers();
  }
 
  deleteProduct(prodcut){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )

  this._productsService.deleteProduct(prodcut);
  }

   ngOnInit() {
        this._productsService.getProducts()
			    .subscribe(products => {this.products = products;
                               this.isLoading = false});
  }

}
