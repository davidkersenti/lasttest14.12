import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';

import{AngularFireModule} from 'angularfire2';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserComponent } from './user/user.component';
import { ProductsComponent } from './products/products.component';
import {  ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';


import { PostsComponent } from './posts/posts.component';
import {  PostsService } from './posts/posts.service';


import { PostComponent } from './post/post.component';
import { InvoicesComponent } from './invoices/invoices.component';
import {  InvoicesService } from './invoices/invoices.service';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PlayersComponent } from './players/players.component';
import {  PlayersService } from './players/players.service';
import { PlayerComponent } from './player/player.component';
import { PositionComponent } from './position/position.component';

import { PostFormComponent } from './post-form/post-form.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieFormComponent } from './movie-form/movie-form.component';
import { MovieComponent } from './movie/movie.component';
import {  MoviesService } from './movies/movies.service';





  
export const firebaseConfig = {
   apiKey: "AIzaSyAybWqWd2Dbr6A6TPi3aZF-t3l5kfSAPoo",
    authDomain: "users-352f2.firebaseapp.com",
    databaseURL: "https://users-352f2.firebaseio.com",
    projectId: "users-352f2",
    storageBucket: "users-352f2.appspot.com",
    messagingSenderId: "914352984198"
}

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'userForm', component: UsersComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'players', component: PlayersComponent },
  { path: 'movies', component: MoviesComponent },
  { path: 'movieForm', component: MovieFormComponent },
  
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoiceForm', component: InvoiceFormComponent },
  { path: 'posts', component: PostsComponent },
  { path: '', component: MoviesComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
     PageNotFoundComponent,
     UserComponent,
      UserFormComponent,
      ProductsComponent,
      ProductComponent,
  
     
      PostsComponent,
    
      PostComponent,
      InvoicesComponent,
      InvoiceFormComponent,
      InvoiceComponent,
      SpinnerComponent,
      PlayersComponent,
      PlayerComponent,
      PositionComponent,
    
      PostFormComponent,
    
      MoviesComponent,
    
      MovieFormComponent,
    
      MovieComponent,
   

  
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
   AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(appRoutes)

  ],
  providers: [UsersService,ProductsService,PostsService,InvoicesService,PlayersService,MoviesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
