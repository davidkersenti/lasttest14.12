import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2'; 

@Injectable()
export class MoviesService {
  moviesObservable;

    constructor(private af:AngularFire) { }
     addMovie(movie){
    this.moviesObservable.push(movie);
  }
   deleteMovie(movie){

  let movieKey = movie.$key;
  this.af.database.object('/movies/' + movieKey).remove();
}
    getMovies(){
    this.moviesObservable = this.af.database.list('/movies');
    return this.moviesObservable;
	}
  updateMovie(movie){
    let movie1 = {name:movie.name,rate:movie.rate,janer:movie.janer}
    console.log(movie1);
    this.af.database.object('/movies/' + movie.$key).update(movie1)
  }

}
