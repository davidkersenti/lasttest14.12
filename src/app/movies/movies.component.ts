import { Component, OnInit } from '@angular/core';
import {MoviesService} from './movies.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
   styles: [`
    .movies li { cursor: default; }
    .movies li:hover { background: #ecf0f1; } 
  `]

})
export class MoviesComponent implements OnInit {
movies;
 currentMovie;

 isLoading = true;
  constructor(private _moviesService: MoviesService) {
    //this.users = this._userService.getUsers();
  }

   addMovie(movie){
    this._moviesService.addMovie(movie);
  }
   deleteMovie(movie){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )

  this._moviesService.deleteMovie(movie);
}
  updateMovie(movie){
     this._moviesService.updateMovie(movie); 
  }

   ngOnInit() {
        this._moviesService.getMovies()
			    .subscribe(movies => {this.movies = movies;
                               this.isLoading = false;
                               console.log(movies)});
  }

}
