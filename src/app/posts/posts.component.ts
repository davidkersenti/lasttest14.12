import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class PostsComponent implements OnInit {

  posts;
  currentPost;
  isLoading = true;

 constructor(private _postsService: PostsService) {

  //  this.posts = this._postsService.getPosts();
  }

   ngOnInit() {
        this._postsService.getPosts()
			    .subscribe(posts => {this.posts = posts;
                               this.isLoading = false});
  }

  addPost(post){
    this._postsService.addPost(post);
  }

   select(post){
		this.currentPost = post; 
    console.log(	this.currentPost);
 }
  
  updatePost(post){
     //this.posts.splice(
     // this.posts.indexOf(post),1,post)
    this._postsService.updatePost(post);
  }

  deletePost(post){
    //this.posts.splice(
      //this.posts.indexOf(post),1)
    this._postsService.deletePost(post);
  }

  
  editPost(originalAndEdited){
    this.posts.splice(

      this.posts.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.posts);
  }  

  
  

}