import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2'; 

@Injectable()
export class PostsService {

    // private _url = "http://jsonplaceholder.typicode.com/posts";
    postsObservable;


  

 getPosts(){
    this.postsObservable= this.af.database.list('/posts').map(
      posts=>{
        posts.map(
          post=>{
            post.userNames=[];
            for(var u in post.users){
              post.userNames.push(
                this.af.database.object('/users/'+u)
              )
            }
         }
        );
        return posts;
      }
    )
    return this.postsObservable;
  }
   addPost(post){
    this.postsObservable.push(post);
  }

  updatePost(post){
    let postKey = post.$key;
    let postData = {id:post.id, title:post.title};
    this.af.database.object('/posts/' + postKey).update(postData);
  }

  
  deletePost(post){
    let postKey = post.$key;
    this.af.database.object('/posts/' + postKey).remove();
   }
 constructor(private af:AngularFire) { }

}