import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {
  invoicesObservable;

   constructor(private af:AngularFire) { }
    getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices').delay(2000)
    return this.invoicesObservable;
 	}
addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }
  deleteInvoice(invoice){

  let invoiceKey = invoice.$key;
  this.af.database.object('/invoices/' + invoiceKey).remove();
}
updateInvoice(invoice){
    let invoice1 = {name:invoice.name,amount:invoice.amount}
    console.log(invoice1);
    this.af.database.object('/invoices/' + invoice.$key).update(invoice1)
  }

}
