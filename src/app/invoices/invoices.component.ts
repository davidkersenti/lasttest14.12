import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
   styles: [`
    .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; } 
  `]
})
export class InvoicesComponent implements OnInit {

  invoices;
  currentInvoice;
  isLoading = true;

  addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }
  constructor(private _invoicesService: InvoicesService) {
    //this.users = this._userService.getUsers();
  }
  deleteInvoice(invoice){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )

  this._invoicesService.deleteInvoice(invoice);
}
updateInvoice(invoice){
     this._invoicesService.updateInvoice(invoice); 
  }

  ngOnInit() {
        this._invoicesService.getInvoices()
			    .subscribe(invoices => {this.invoices = invoices;
                               this.isLoading = false});
  }

}
