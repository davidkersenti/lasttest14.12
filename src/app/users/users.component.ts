import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users;
  

 currentUser;

 isLoading = true;

  constructor(private _usersService: UsersService) {
    //this.users = this._userService.getUsers();
  }
   addUser(user){
    this._usersService.addUser(user);
  }
deleteUser(user){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )

  this._usersService.deleteUser(user);
}
updateUser(user){
     this._usersService.updateUser(user); 
  }
  ngOnInit() {
        this._usersService.getUsers()
			    .subscribe(users => {this.users = users;
                               this.isLoading = false;
                               console.log(users)});
  }

}
