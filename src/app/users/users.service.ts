import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2'; 


@Injectable()
export class UsersService {
   usersObservable;

  constructor(private af:AngularFire) { }

   addUser(user){
    this.usersObservable.push(user);
  }

  getUsers(){
    this.usersObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return users;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}
  deleteUser(user){

  let userKey = user.$key;
  this.af.database.object('/users/' + userKey).remove();
}
updateUser(user){
    let user1 = {name:user.name,email:user.email}
    console.log(user1);
    this.af.database.object('/users/' + user.$key).update(user1)
  } 

  

}
